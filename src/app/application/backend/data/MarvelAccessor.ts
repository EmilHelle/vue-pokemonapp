import axios from "axios";
const crypto = require('crypto');

class MarvelAccessor {
  private api: string;
  private publicKey: string;
  private privateKey:string;

  private ts;
  private createdHash;
  private hash;
  constructor() {
    this.api = "https://gateway.marvel.com/v1/public/";
    this.publicKey = "dcbe85cc1e39dd036e85dfd88f925bd9";
    this.privateKey = "c21c9b95f4e09b37aa238928c2da22453e7be00c";

    this.ts = Date.now().toString();
    this.createdHash = crypto.createHash("md5");
    this.hash = this.createdHash.update(this.ts + this.privateKey + this.publicKey).digest("hex");
    console.log("creating new instance of MarvelAccessor");
  }

  getAllCharacters(offset?: number) {
    const o = offset || "0";
    return axios.get(`${this.api}characters`, {
      params: {
        ts: this.ts,
        apikey: this.publicKey,
        hash: this.hash,
        offset: o,
        limit: "50"
      }
    });
  }

  getCharacter(id: number) {
    return axios.get(`${this.api}characters/${id}`, {
      params: {
        ts: this.ts,
        apikey: this.publicKey,
        hash: this.hash,
      }
    });
  }

  getAllComics(offset?: number) {
    const o = offset || "0";
    return axios.get(`${this.api}comics`, {
      params: {
        ts: this.ts,
        apikey: this.publicKey,
        hash: this.hash,
        offset: o,
        limit: "50"
      }
    });
  }

  getComic(id: number) {
    return axios.get(`${this.api}comics/${id}`, {
      params: {
        ts: this.ts,
        apikey: this.publicKey,
        hash: this.hash,
      }
    });
  }

  getAllSeries() {
    return axios.get(`${this.api}/series`);
  }

  getSeries(id: number) {
    return axios.get(`${this.api}/series/${id}`);
  }
}

// Export a singleton instance in the global namespace
const marvelAccessor = new MarvelAccessor();

export default marvelAccessor;
