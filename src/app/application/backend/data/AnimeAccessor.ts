import axios from "axios";

class AnimeAccessor {
  constructor() {
    console.log("creating new instance of AnimeAccessor");
  }

  getAllCharacters() {
    return axios.get("https://kitsu.io/api/edge/characters?page[limit]=20&page[offset]=100");
  }

  getCharacter(id: number) {
    return axios.get(`https://kitsu.io/api/edge/characters/${id}`);
  }

  getAllMangas() {
    return axios.get("https://kitsu.io/api/edge/manga?page[limit]=20&page[offset]=100");
  }

  getManga(id: number) {
    return axios.get(`https://kitsu.io/api/edge/manga/${id}`);
  }

}

// Export a singleton instance in the global namespace
const animeAccessor = new AnimeAccessor();

export default animeAccessor;
