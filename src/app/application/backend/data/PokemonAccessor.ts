import axios from "axios";

class PokemonAccessor {
  constructor() {
    console.log("creating new instance of PokemonAccessor");
  }

  getAllPokemons() {
    return axios.get("https://pokeapi.co/api/v2/pokemon/?offset=210&limit=50");
  }

  getPokemon(name: string) {
    return axios.get(`https://pokeapi.co/api/v2/pokemon/${name}`);
  }

  getAllBerries() {
    return axios.get("https://pokeapi.co/api/v2/berry/?offset=0&limit=100");
  }

  getBerry(name: string) {
    return axios.get(`https://pokeapi.co/api/v2/berry/${name}`);
  }

  getAllItems() {
    return axios.get("https://pokeapi.co/api/v2/item");
  }

  getItem(name: string) {
    return axios.get(`https://pokeapi.co/api/v2/item/${name}`);
  }

}

// Export a singleton instance in the global namespace
const pokemonAccessor = new PokemonAccessor();

export default pokemonAccessor;
